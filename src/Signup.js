import React, { Component } from 'react'
import M from "materialize-css"

export default class Signup extends Component {

    constructor(){
      super();
      this.state = {
        canChooseRole:true,
        username:'',
        email:'',
        password:'',
        confirmPassword:'',
        gender:'male',
        dob:'',
        profilePictureUrl:'',
        listOfCountries:[
          {
            country:"Choose country",
            value:"",
          },
          {
            country:"India",
            value:"india",
          },
          {
            country:"US",
            value:"us",
          },
          {
            country:"German",
            value:"german",
          },
        ],
        country:"",
        isAdmin:false
      }
    }



  componentDidMount() {
    // dropdown init
    var elems = document.querySelectorAll('select');
    M.FormSelect.init(elems, {});

    //date picker init
    var elems = document.querySelectorAll('.date-picker');

    let datePickerOptions = {
      onSelect:(date)=>{
        console.log(date)
          this.handleSignupFormChange({
            target:{
              name:"dob",
              value:date
            }
          })
      }
    }
    M.Datepicker.init(elems, datePickerOptions);
  }

  handleSignupFormChange(e){
    
    let changingObject = {
      ...this.state,
    }

    changingObject[e.target.name] = e.target.value;

    this.setState({
      ...changingObject
    }, ()=>{

      if(e.target.name == "confirmPassword"){
        if(this.state.password == this.state.confirmPassword){
            M.toast({
              html:"password matching",
              classes:"green rounded"
            })
        } 
      }

      console.log(this.state)
    })

    
  }

  handleGenderChange(e){
    console.log(e);
    if(e.target.checked){
      this.setState({
        gender:e.target.value
      }, ()=>{
        console.log(this.state)
      })
    }
  }

  handleAdminChange(e){
    
    this.setState({
      isAdmin:e.target.checked
    })
  }

  handleSubmit(e){
    e.preventDefault();
    console.log(this.state)
  }

  toggle(e){
    this.setState({
      canChooseRole:!(this.state.canChooseRole)
    })
  }




  render() {
    return (
      <div className="container">
        <form onSubmit={(e)=>this.handleSubmit(e)} >
          <div className="row">
            <div className=" col s6 offset-s3">
              <div className="card signup-card">
                <div className="card-content">
                  <h4 className="center-align" onClick={(e)=>this.toggle(e)} >Join us!</h4>

                  <div className='row'>
                    <div className="col s12 input-field">
                      <i className='material-icons prefix'>account_circle</i>
                      <label htmlFor="username_ib">Full Name</label>
                      <input 
                      type="text" 
                      id="username_ib" 
                      required 
                      name="username"
                      className="validate"
                      onChange={(e)=>this.handleSignupFormChange(e)}  />
                      <span className="helper-text" data-error="Please fill thsi field" data-success="good to go"></span>
                    </div>
                  </div>

                  <div className='row'>
                    <div className="col s12 input-field">
                      <i className='material-icons prefix'>email</i>
                      <label htmlFor="email_ib">Email</label>
                      <input type="email" name='email' 
                      onChange={(e)=>this.handleSignupFormChange(e)}
                      id="email_ib" required className="validate" />
                    </div>
                  </div>


                  <div className='row'>
                    <div className="col s6 input-field">
                      <i className='material-icons prefix'>lock</i>
                      <label htmlFor="password_ib">Password</label>
                      <input type="password" id="password_ib" 
                      onChange={(e)=>this.handleSignupFormChange(e)}
                      name="password"
                      required className="validate"  />
                    </div>

                    <div className="col s6 input-field">
                      <i className='material-icons prefix'>lock</i>
                      <label htmlFor="c_password_ib">Confirm passsword</label>
                      <input type="password" id="c_password_ib" 
                      onChange={(e)=>this.handleSignupFormChange(e)}
                      name="confirmPassword"
                      required className="validate" />
                    </div>
                  </div>

                  <div className="row">
                    <div className="col s4">
                      <p>Gender</p>
                    </div>
                    <div className="col s4">
                      <p>
                        <label>
                          <input type="radio" required className="validate"
                          onChange={(e)=>this.handleGenderChange(e)}
                           name="gender" value="male" />
                          <span>Male</span>
                        </label>
                      </p>
                    </div>
                    <div className="col s4">
                      <p>
                        <label>
                          <input type="radio" 
                          onChange={(e)=>this.handleGenderChange(e)}
                          required className="validate" name="gender" value="female" />
                          <span>Female</span>
                        </label>
                      </p>
                    </div>
                  </div>

                  <div className="row">
                    <div className='col s6 input-field'>
                      <i className='material-icons prefix'>date_range</i>
                      <label htmlFor="dob_dd" required className="validate" >Date of Birth</label>
                      <input type="text" className="date-picker" id="dob_dd" 
                      onChange={(e)=>this.handleSignupFormChange(e)}
                      name="dob" />
                    </div>

                    <div className='col s6 input-field'>
                      <i className='material-icons prefix'>edit_location</i>
                      <label htmlFor="dob_dd" className='active' >Country</label>
                      <select 
                      onChange={(e)=>this.handleSignupFormChange(e)}
                      name="country">
                        {
                          this.state.listOfCountries.map((country, index)=>{
                            return (
                              <option key={country.value} value={country.value} > {country.country} </option>
                            )
                          })
                        }
                      </select>
                    </div>
                  </div>

                  <div className='row'>
                    <div className="col s8 input-field">
                      <div className="file-field">
                        <div className='btn-small white black-text'>
                          <span>Profile Picture</span>
                          <input type="file" 
                          name="profilePictureUrl"
                          onChange={(e)=>this.handleSignupFormChange(e)}/>

                        </div>
                        <div className="file-path-wrapper">
                          <input className="file-path validate" type="text" 
                          name="profilePictureUrl"
                          onChange={(e)=>this.handleSignupFormChange(e)}/>
                        </div>
                      </div>
                    </div>


                        {
                          this.state.canChooseRole ? 
                          (
                            <div className="col s4 input-field">
                      <p>
                        <label>
                          <input type="checkbox" name="isAdmin" onChange={(e)=>this.handleAdminChange(e)}/>
                          <span>Is Admin ? </span>
                        </label>
                      </p>
                    </div>
                          ) : ''
                        }
                    


                  </div>

                </div>

                <div className="card-action">
                      <div className="center-align">
                      <input type="submit" value="Sign-up" className="btn" />
                      </div>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    )
  }
}
