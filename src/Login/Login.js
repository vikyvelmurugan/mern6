import React, { Component } from 'react'
import Message from './Message';

export default class Login extends Component {

    constructor(props){
        console.log("constructor in Login")
        super(props);
        this.state = {
            name : "Vignesh"
        }
    }

    // static getDerivedStateFromProps(newProps, previousState){
    //     console.log("getDerivedStateFromProps in Login")
    //     return {
    //         ...previousState,
    //         name : newProps.name
    //     }
    // }

    componentDidMount(){
        console.log("componentDidMount in Login")
    }

    

    componentDidUpdate(){
        console.log("componentDidUpdate in Login")
    }

    childBtnClick = ()=>{
        // document.getElementById("x").remove();
        this.setState({
            name : " Sachin"
        })
    }

  render() {
      console.log("rendering in Login")
    return (
      <div> 
          
          {this.state.name} 
      
     <div id="x">
     <Message message = {this.state.name} onBtnClick = {()=>this.childBtnClick()} />
     </div>
      
      </div>
      
    )
  }
}
