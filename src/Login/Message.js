import React, { Component } from 'react'

export default class Message extends Component {

    oldProps = []
    oldState = []


    constructor(props) {
        console.log("constructor in Message")
      super(props)
        this.state = {
            message : ''
        }
    }

 

    componentDidMount(){
        console.log("componentDidMount in Message")
    }

    // shouldComponentUpdate(){
    //     return false;
    // }

    componentDidUpdate(){
        console.log("componentDidUpdate in Message")
    }

    componentWillUnmount(){
        alert("sdfsdfds")
    }

    onBtnClick = ()=>{
        this.props.onBtnClick()
        // this.props.message = "Sachin";
        console.log(this.oldProps)
        console.log(this.oldState)
    }

    getSnapshotBeforeUpdate(prevProps, pevState){
        this.oldProps.push(prevProps)
        this.oldState.push(pevState)
    }


  render() {
      console.log("Rendering in Message")
    return (
      <div> 
          
          <button onClick={this.onBtnClick} >Click me</button>
          {this.props.message} </div>
    )
  }
}
